package programa;

import java.util.*;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Matriz de Adjacencia
		System.out.println("Matriz de Adjacencia!");
		System.out.println();
		MatrizAdjacencia matrizAd = new MatrizAdjacencia();
		int[][] matrizAdjacencia = matrizAd.buildMatrizAdjacencia();
		matrizAd.mostrarMatrizAdjacencia(matrizAdjacencia);
		System.out.println(matrizAd.vertices);

		// Lista de Adjacencia
		System.out.println("============");
		System.out.println("Lista de Adjacencia!");
		System.out.println();
	    
		// Criando o grafo
	    int V = 5;
	    ArrayList<ArrayList<Integer>> am = new ArrayList<ArrayList<Integer>>(V);

	    for (int i = 0; i < V; i++)
	      am.add(new ArrayList<Integer>());

	    // Adicionando Vertices
	    ListaAdjacencia.addVertice(am, 0, 1);
	    ListaAdjacencia.addVertice(am, 0, 2);
	    ListaAdjacencia.addVertice(am, 0, 3);
	    ListaAdjacencia.addVertice(am, 1, 2);

	    ListaAdjacencia.printGrafo(am);
	}

}
