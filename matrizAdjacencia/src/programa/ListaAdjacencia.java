package programa;
import java.util.*;

public class ListaAdjacencia {

	  // Adicionar vertice
static void addVertice(ArrayList<ArrayList<Integer>> am, int s, int d) {
	    am.get(s).add(d);
	    am.get(d).add(s);
	  }
	  // Mostrar o grafo
	  static void printGrafo(ArrayList<ArrayList<Integer>> am) {
	    for (int i = 0; i < am.size(); i++) {
	      System.out.println("\nVertex " + i + ":");
	      for (int j = 0; j < am.get(i).size(); j++) {
	        System.out.print(" -> " + am.get(i).get(j));
	      }
	      System.out.println();
	    }
	  }
	}

