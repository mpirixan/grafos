package programa;

public class MatrizAdjacencia {

		public static final int VERTICES_QTD = 4;								//quantidade de vértices no grafo
		public char[] vertices = new char[VERTICES_QTD];							//array com os vértices do grafo

		MatrizAdjacencia(){
			vertices[0] = 'A';
			vertices[1] = 'B';
			vertices[2] = 'C';
			vertices[3] = 'D';
		}

		public int[][] buildMatrizAdjacencia(){
			int[][] matrizAdjacencia = new int[VERTICES_QTD][VERTICES_QTD];			//Matriz NxN
			this.iniciarMatrizAdjacencia(matrizAdjacencia);

			matrizAdjacencia[0][1] = 1;										//AB
			matrizAdjacencia[0][3] = 1;										//AD

			matrizAdjacencia[1][0] = 1;										//BA
			matrizAdjacencia[1][2] = 1;										//BC
			matrizAdjacencia[1][3] = 1;										//BD

			matrizAdjacencia[2][1] = 1;										//CB

			matrizAdjacencia[3][0] = 1;										//DA
			matrizAdjacencia[3][1] = 1;										//DB

			return matrizAdjacencia;		
		}

		public void iniciarMatrizAdjacencia(int[][] matrizAdjacencia){
			for(int i=0; i<matrizAdjacencia.length; i++){
				for(int j=0; j<matrizAdjacencia[i].length; j++){
					matrizAdjacencia[i][j] = 0;								//Inicialização da matriz
				}
			}
		}

		public void mostrarMatrizAdjacencia(int[][] matrizAdjacencia){
			for(int i=0; i<matrizAdjacencia.length; i++){					  //itero as linhas
				for(int j=0; j<matrizAdjacencia[i].length; j++){				//itero as colunas
	      if(matrizAdjacencia[i][j] != 0){							          //quero imprimir somente as ligações
						System.out.println(vertices[i] + "->" + vertices[j]);		  //Imprime as arestas
					}
				}
			}
		}
}
	
	

