// Base em pdf
package app;

import java.util.Queue;

public class BuscaEmLargura {

	private boolean[] marcado;
	public BuscaEmLargura(Graph G, int s) {
	marcado = new boolean[G.V()];
	bfs(G,s);
	}
	private void bfs(Graph G, int s) {
	Queue<Integer> queue = new Queue<Integer>();
	marcado[s] = true;
	((Object) queue).enqueue(s);
	while( !q.vazio() ) {
	int v = ((Object) queue).dequeue();
	for (int w: G.adj(v))
	if ( !marcado[w] ) {
	marcado[w] = true;
	((Object) queue).enqueue(w);
	}
	}
	} 
	public boolean marcado(int w){
		return marcado[w];
	}
}
