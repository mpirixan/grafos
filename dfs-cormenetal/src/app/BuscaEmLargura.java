package app;

public class BuscaEmLargura {
    private boolean [] marcado;
    private int cont;
    public BuscaEmLargura(Grafo G, int s){
        marcado = new boolean[G.V()];
        dfs(G,s);
    }
private void dfs(Grafo G, int v){
        marcado[v] = true;
        cont++;
        for (int w: G.adj())
            if (!marcado[w] dfs(G,w));
}
}
