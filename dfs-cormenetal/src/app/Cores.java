package app;

public enum Cores {
    BRANCO("branco"),
    CINZA("cinza"),
    PRETO("preto");

    private String value;

    public String getValue() {
        return this.value;
    }


    private Cores(String value) {
        this.value = value;
    }


    public String getKey() {
        return this.name();
    }

}
